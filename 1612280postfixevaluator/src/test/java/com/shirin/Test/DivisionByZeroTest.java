
package com.shirin.Test;

import com.shirin.evaluator.Evaluator;
import com.shirin.evaluator.exceptions.DivisionByZeroException;
import com.shirin.evaluator.exceptions.NoneBinaryException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Shirin Eskandari
 */
@RunWith(Parameterized.class)
public class DivisionByZeroTest {
 
     private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(DivisionByZeroTest.class);
    private final Evaluator evaluator;
    private final Queue<String> infix;
    public Exception expected;
    private Queue<String> postfix;
    private String result;

    @Parameterized.Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"( 6 / 0 ) - 3"},
            {"( 6 / 0 ) + 3"},
            {"( 6 / 0 )"}

        });
    }

    public DivisionByZeroTest(String expression) {
              LOG.info("DivisionByZeroTest");
        infix = new LinkedList<>();
        expected = new NoneBinaryException();
        String[] array = expression.split(" ");
        boolean addAll = infix.addAll(Arrays.asList(array));
        evaluator = new Evaluator();
    }

    @Test(expected = DivisionByZeroException.class)
    public void evaluateTest() throws DivisionByZeroException {
        postfix = evaluator.convertToPostfix(infix);
        evaluator.evaluate(postfix);
        fail(expected.getMessage());
    }

}

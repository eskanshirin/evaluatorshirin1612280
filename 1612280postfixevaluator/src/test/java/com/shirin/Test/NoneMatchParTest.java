
package com.shirin.Test;

import com.shirin.evaluator.Evaluator;
import com.shirin.evaluator.exceptions.NoneBinaryException;
import com.shirin.evaluator.exceptions.NoneMatchingParenthesisException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Shirin Eskandari
 */
@RunWith(Parameterized.class)
public class NoneMatchParTest {

        private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NoneMatchParTest.class);
    private final Evaluator evaluator;
    private final Queue<String> infix;
    public Exception expected;
    private Queue<String> postfix;
    private String result;

    @Parameterized.Parameters(name = "{index} plan[{0}]={1}]")

    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"5 + ( ( 1 * 2 ) + 3"},
            {"5 + ( ( ( 1 * 2 ) + 3"},
            {"5 ( 2 + ( 1 * 2 ) + 3"}
        });
    }

    public NoneMatchParTest(String expression) {
        LOG.info("NoneMatchParTest");
        infix = new LinkedList<>();
        expected = new NoneBinaryException();
        String[] array = expression.split(" ");
        infix.addAll(Arrays.asList(array));
        evaluator = new Evaluator();
    }

    @Test(expected = NoneMatchingParenthesisException.class)
    public void evaluateTest() throws NoneMatchingParenthesisException {
        postfix = evaluator.convertToPostfix(infix);
        evaluator.evaluate(postfix);

        fail(expected.getMessage());
    }

}

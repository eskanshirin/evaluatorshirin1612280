
package com.shirin.Test;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.shirin.evaluator.Evaluator;
import java.util.LinkedList;
import java.util.Queue;

@RunWith(Parameterized.class)
public class EvaluatorTest {
     private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(EvaluatorTest.class);

    private final Evaluator evaluator;
    private final Queue<String> infix;
    private final Queue<String> expected;
    private Queue<String> postfix;
    private final String result;

    /*
     * Shirin Eskandari
     */
    @Parameterized.Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"10 - ( 2 / 2 + 3 - ( 10 + 27 ) ) - 5", "10 2 2 / 3 + 10 27 + - - 5 -", "38.0"},
            {"2 * 3 + 4", "2 3 * 4 +", "10.0"},
            {"( 13 + 27 ) * 2 - 34 / 17 + 2 * 7", "13 27 + 2 * 34 17 / - 2 7 * +", "92.0"},
            {"( 5 + 1 ) * 2 + 3", "5 1 + 2 * 3 +", "15.0"},
            {"5 + ( 1 * 2 ) + 3", "5 1 2 * + 3 +", "10.0"},
            {"( 22 / 11 + 3 - 7 ) * 12 - ( 2 + 1 )", "22 11 / 3 + 7 - 12 * 2 1 + -", "-27.0"},
            {"1 / 0.4 + 2.3 - ( -10 )", "1 0.4 / 2.3 + -10 -", "14.8"},
            {"11 + 1 - 100 * 2 / ( 50 - 25 ) + 25", "11 1 + 100 2 * 50 25 - / - 25 +", "29.0"},
            {"11 + ( ( 1 - 100 ) * 2 / ( 50 - 25 ) ) + 25", "11 1 100 - 2 * 50 25 - / + 25 +", "28.08"},
            {"2.3 * 3 + 4.7", "2.3 3 * 4.7 +", "11.6"},
            {"1.4 * 2 - 2.3 + ( -17.3 )", "1.4 2 * 2.3 - -17.3 +", "-16.8"},
            {"( ( 3 + 11 ) / ( 2 + 8 ) - 3 ) * 2", "3 11 + 2 8 + / 3 - 2 *", "-3.2"},
            {"1 / 0.4 + 2.3 - ( -10 )", "1 0.4 / 2.3 + -10 -", "14.8"},
            {"( 2 + 8 * 2 / 4 ) - ( 3 + 7 ) / 2 + 7", "2 8 2 * 4 / + 3 7 + 2 / - 7 +", "8.0"},
            {"( 22 / 11 + 3 - 7 ) * 12 - ( 2 + 1 )", "22 11 / 3 + 7 - 12 * 2 1 + -", "-27.0"},
            {"( -10 * 8 ) * 100 + 21 / 7", "-10 8 * 100 * 21 7 / +", "-7997.0"},
            {"2 + 2 + 2 + 2 + 2", "2 2 + 2 + 2 + 2 +", "10.0"},
            {"100 / 10 * 2.5 + ( -20 )", "100 10 / 2.5 * -20 +", "5.0"},
            {"10 - ( 2 / 2 + 3 - ( 10 + 27 ) ) - 5", "10 2 2 / 3 + 10 27 + - - 5 -", "38.0"},
            {"( 1 + ( 3 + ( 4 - 5 ) * 3 ) - 5 ) - 10", "1 3 4 5 - 3 * + + 5 - 10 -", "-14.0"},
            {"( ( ( ( ( 2 + 7 ) - 5 * 2 ) / 2 ) - 2 ) + 10 ) * 3", "2 7 + 5 2 * - 2 / 2 - 10 + 3 *", "22.5"},
            {"10 / 2", "10 2 /", "5.0"},
            {"22 / 2 + 2 * 2 - 2 + 2", "22 2 / 2 2 * + 2 - 2 +", "15.0"},
            {"( ( 12 - 10 ) / 5 + ( 33 + 11 ) - 15 ) * 23", "12 10 - 5 / 33 11 + + 15 - 23 *", "676.2"},
            {"1.4 * 2 - 2.3 + ( -17.3 )", "1.4 2 * 2.3 - -17.3 +", "-16.8"}
        });
    }

    public EvaluatorTest(String expression, String expect, String result) {
         LOG.info("EvaluatorTest");
        infix = new LinkedList<>();
        expected = new LinkedList<>();
        String[] array = expression.split(" ");
        infix.addAll(Arrays.asList(array));
        array = expect.split(" ");
        expected.addAll(Arrays.asList(array));
        evaluator = new Evaluator();
        this.result = result;
    }

    @Test
    public void convertTest() {
        while (!postfix.isEmpty()) {
            assertEquals(expected.remove(), postfix.remove());
        }
    }

    @Test
    public void evaluateTest() {
        double received = Double.parseDouble(evaluator.evaluate(postfix).remove());
        String rcvResult = (Math.round(received * 100.0) / 100.0) + "";
        assertEquals(result, rcvResult);
    }

    @Before
    public void init() {
        postfix = evaluator.convertToPostfix(infix);
    }

}

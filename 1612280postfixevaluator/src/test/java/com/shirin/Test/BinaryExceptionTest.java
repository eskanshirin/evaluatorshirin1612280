/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shirin.Test;

import com.shirin.evaluator.Evaluator;
import com.shirin.evaluator.exceptions.MainApp;
import com.shirin.evaluator.exceptions.NoneBinaryException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/**
 *
 * @author shirin Eskandari
 */

    @RunWith(Parameterized.class)
public class BinaryExceptionTest {
  private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(BinaryExceptionTest.class);
    private final Evaluator evaluator;
    private final Queue<String> infix;
    public Exception expected;
    private Queue<String> postfix;
    private String result;

    @Parameterized.Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
           
            { "( 5 + + 1 ) * 2 + 3"},   
            { "( ( 5 + 1 1 ) * 2 ) + 3"},
            {"2 + 3 *"}

           });
}
    
      public BinaryExceptionTest(String expression) {
          LOG.info("BinaryExceptionTest");
        infix = new LinkedList<>();
        expected = new  NoneBinaryException();
        String[] array = expression.split(" ");
        boolean addAll = infix.addAll(Arrays.asList(array));
        evaluator = new Evaluator();
    }

      @Test(expected=NoneBinaryException.class)
    public void evaluateTest()throws NoneBinaryException {      
 postfix = evaluator.convertToPostfix(infix);
        evaluator.evaluate(postfix);

      fail(expected.getMessage());
    }
    
    }


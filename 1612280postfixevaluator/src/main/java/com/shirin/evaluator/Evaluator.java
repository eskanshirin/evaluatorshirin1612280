
package com.shirin.evaluator;
import com.shirin.evaluator.exceptions.*;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Evaluator use postfix  to evaluate equations 
 * it uses queue and dequeue java classes to calculate
 * @author Shirin Eskandari
 */
public class Evaluator {
     private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(Evaluator.class);
    private Queue<String> infix;
    private Queue<String> postfix;
    private Deque<String> operators;
    private Deque<String> values;
    
   
    /**
     * 
     * This method converts infix to postfix, and returns postfix 
     * @param infix
     * @return 
     */
    public Queue<String> convertToPostfix(Queue<String> infix) {
        LOG.info("inside convertToPostfix ");
        this.infix = infix;
        postfix = new LinkedList<>();
        operators = new LinkedList<>();
        values = new LinkedList<>();
        convert();             
        return postfix;
    }
     /**
     * Determines if the given string is a valid number.
     *
     * @param num
     * @return
     */
    private boolean isNumber(String num) {
        try {
            Double d = Double.parseDouble(num);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * this method evaluates postfix, if it is a number , 
     * or an operator and it will add them to values, also checks if operators 
     * are valid
     * @param postfix
     * @return resultQueue
     * @throws NoneValidStringInInputQueueException
     */
    public Queue<String> evaluate(Queue<String> postfix) {
        LOG.info("inside evaluate ");
        double result;
        Queue<String> resultQueue = new LinkedList<>();
        while(!postfix.isEmpty()) {
            String value = postfix.remove();
          
          if(isNumber(value)){
                values.push(value);
            }
            else if(isOperator(value)) {
                double rightValue = getValue();
                double leftValue = getValue();
                result = calculate(leftValue, value, rightValue);
                values.push(result+"");
            }
            else {
                throw new NoneValidStringInInputQueueException();
            }
        }     
        resultQueue.add(values.pop());
        if(!values.isEmpty())
            throw new NoneBinaryException();
        return resultQueue;
    }
 
    /**
     * It gets value of postfix 
     * @return 
     * @throws NoneBinaryException
     */
    private double getValue() {
        if(values.isEmpty())
            throw new NoneBinaryException();                   
        return Double.parseDouble(values.pop());
    }
  
    /**
     * It evaluates the operators are + - * /
     * @param value
     * @return 
     */
    private boolean isOperator(String value) {
        return value.equals("+") || value.equals("-")
            || value.equals("*") || value.equals("/");
    }
   
    /**
     * it is helper method to convert infix to postfix and 
     * it counts left and right parentheses ,pop the left operators from the stack
     * @throws NoneMatchingParenthesisException, NoneValidStringInInputQueueException
     */
    private void convert() {      
        int rightParentheses = 0;
        int leftParentheses = 0;
        
        while(!infix.isEmpty()) {
            String value = infix.remove();
            if(value.equals("(")) {
                leftParentheses++;
                operators.push(value);
            }
            else if (value.equals(")")) {
                rightParentheses++;
                handleRightParenthesis();              
            }
            else if(isOperator(value)) {
                handleOperator(value);
            }
       
             else  if(isNumber(value)){
                postfix.add(value);
            }
            else {
                throw new NoneValidStringInInputQueueException();
            }
        }
        while(!operators.isEmpty()) {
            postfix.add(operators.pop());
        }
        if(rightParentheses != leftParentheses)
            throw new NoneMatchingParenthesisException();
    }
 
    /**
     * This method handles operators, on base of their precedence,
     * 
     * @param nextOperator 
     */
    private void handleOperator(String nextOperator) {     
        if(!operators.isEmpty()) {
            String prevOperator = operators.peek();
            while(!prevOperator.equals("(") && changeOrder(prevOperator, nextOperator)) {
                postfix.add(prevOperator);
                checkOperators();
                operators.pop();
                prevOperator = operators.isEmpty() ? "(" : operators.peek();        
            }
        }
        operators.push(nextOperator);
    }
  
    /**
     * private method to handle right (
     */
    private void handleRightParenthesis() {   
          LOG.info("inside handleRightParenthesis");
        String operator = operators.pop();
        if(!operator.equals("(")) {
            if(!isOperator(operator))
                throw new NoneValidStringInInputQueueException();
            else {
                postfix.add(operator);
                checkOperators();
                while(!operators.peek().equals("(")) {
                    String value = operators.pop();
                    postfix.add(value);
                    checkOperators();
                }           
                operators.pop();
            }
        }
    }
   
    /**
     * it throws exception if operators is empty
     * throws NoneValidStringInInputQueueException
     */
    private void checkOperators() {
          LOG.info("inside checkOperators");
        if(operators.isEmpty())
            throw new NoneValidStringInInputQueueException();
    }
 
    /**
     * it will check precedence of operators and if an operator has more
     * precedence, the more precedence one will change place with less
     * @param prevOperator
     * @param nextOperator
     * @return 
     *  @throws NoneValidStringInInputQueueException
     */
    private boolean changeOrder(String prevOperator, String nextOperator) {
        if(nextOperator.equals("+") || nextOperator.equals("-")) {
            return true;            
        }
        if(nextOperator.equals("*") || nextOperator.equals("/")) {
            return prevOperator.equals("*") || prevOperator.equals("/");
        }
        else
            throw new NoneValidStringInInputQueueException();
    }

    /**
     * it will do calculation on numbers
     * @param leftValue
     * @param operator
     * @param rightValue
     * @return 
     * throw  NoneValidStringInInputQueueException();
     */
    private double calculate(double leftValue, String operator, double rightValue) {
        LOG.info("inside calculate");
        if(operator.equals("+")){
            return leftValue+rightValue;            
        } else if(operator.equals("-")){
            return leftValue-rightValue;
        }else if((operator.equals("/"))){
            if(rightValue == 0){
                throw new DivisionByZeroException();
            }
            return leftValue/rightValue;            
        }else if(operator.equals("*")){
            return leftValue*rightValue;
        } else{
            throw new NoneValidStringInInputQueueException();
        }        
    }

}

package com.shirin.evaluator.exceptions;

/**
 *
 * @author Shirin Eskandari
 */
public class NoneValidStringInInputQueueException extends RuntimeException{
    private static final long serialVersionUID = 43051768871L;
    public NoneValidStringInInputQueueException() {
        super("The character in the input  is not correct.");
    }
    
}

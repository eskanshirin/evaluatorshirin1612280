
package com.shirin.evaluator.exceptions;


/**
 *
 * @author Shirin Eskandari
 */
public class NoneBinaryException extends RuntimeException{
    private static final long serialVersionUID = 42051758871L;
    public NoneBinaryException() {
    
        super("Binary operands don't match");
    }
    
    
}

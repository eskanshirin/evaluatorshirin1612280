
package com.shirin.evaluator.exceptions;

/**
 *
 * @author Shirin Eskandari
 */
public class DivisionByZeroException extends RuntimeException {
     private static final long serialVersionUID = 49051758871L;
    public DivisionByZeroException() {
    
        super("Division by zero");

    }
    
}

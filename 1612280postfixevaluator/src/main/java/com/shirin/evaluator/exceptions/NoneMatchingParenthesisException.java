
package com.shirin.evaluator.exceptions;

/**
 *
 * @author Shirin Eskandari
 */
public class NoneMatchingParenthesisException extends RuntimeException{
    private static final long serialVersionUID = 42151768871L;
    public NoneMatchingParenthesisException() {
        super("Parentheses are not match.");
    }
    
}
